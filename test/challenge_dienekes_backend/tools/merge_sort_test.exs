defmodule ChallengeDienekesBackend.Tools.MergeSortTest do
  use ExUnit.Case, async: true

  alias ChallengeDienekesBackend.Tools.MergeSort
  alias ChallengeDienekesBackendWeb.Integrations.DienekesApi

  describe "Verifica se as listas estão corretamente ordenadas após aplicação da função" do
    for t <- 1..10 do
      test "Teste #{t}" do
        page = unquote(t)

        %{"numbers" => list} = DienekesApi.fetch_numbers(page)

        assert MergeSort.merge_sort(list) == Enum.sort(list)
      end
    end
  end
end
