defmodule ChallengeDienekesBackendWeb.NumberControllerTest do
  @moduledoc false

  use ChallengeDienekesBackendWeb.ConnCase

  describe "index - Renderizar dados do endpoint principal - Ordenado apenas após obter todas as páginas" do
    for page <- 1..10 do
      test "Page #{page}", %{conn: conn} do
        conn = get(conn, Routes.number_path(conn, :index, %{page: unquote(page), page_size: 100}))
        assert is_list(json_response(conn, 200)["numbers"])
      end
    end
  end

  describe "loading - Renderizar dados do endpoint de testes, em tempo de carregamento - Ordenado em cada requisição" do
    for page <- 1..10 do
      test "Page #{page}", %{conn: conn} do
        conn =
          get(conn, Routes.number_path(conn, :loading, %{page: unquote(page), page_size: 100}))

        assert is_list(json_response(conn, 200)["numbers"])
      end
    end
  end
end
