defmodule ChallengeDienekesBackendWeb.Integrations.DienekesApiTest do
  use ExUnit.Case, async: true

  alias ChallengeDienekesBackendWeb.Integrations.DienekesApi

  # @doctest DienekesApi

  describe "Testando retorno da API" do
    for t <- 1..200 do
      test "Teste #{t}" do
        page = unquote(t)

        {:ok, conn} = DienekesApi.get("/api/numbers?page=#{page}")

        assert conn.status == 200
      end
    end
  end
end
