defmodule ChallengeDienekesBackendWeb.NumberController do
  use ChallengeDienekesBackendWeb, :controller

  alias ChallengeDienekesBackend.NumberCollection

  @accepts ~w(page page_size)
  @spec index(Plug.Conn.t(), map) :: Plug.Conn.t()
  def index(conn, params) do
    params = Map.take(params, @accepts)

    resp = NumberCollection.value(params) |> Enum.into(%{})

    json(conn, resp)
  end

  @spec loading(Plug.Conn.t(), map) :: Plug.Conn.t()
  def loading(conn, params) do
    params = Map.take(params, @accepts)

    resp = NumberCollection.sorted_value(params) |> Enum.into(%{})

    json(conn, resp)
  end
end
