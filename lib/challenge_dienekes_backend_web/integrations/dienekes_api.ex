defmodule ChallengeDienekesBackendWeb.Integrations.DienekesApi do
  use Tesla

  plug Tesla.Middleware.BaseUrl, "http://challenge.dienekes.com.br"
  plug Tesla.Middleware.JSON

  plug Tesla.Middleware.Retry,
    delay: 500,
    max_retries: 5,
    max_delay: 4_000,
    should_retry: fn
      {:ok, %{status: status}} when status in [400, 500] -> true
      {:ok, _} -> false
      {:error, _} -> true
    end

  @spec fetch_numbers(integer | binary) :: map | atom
  def fetch_numbers(page) do
    get("/api/numbers?page=#{page}")
    |> response_body()
  end

  defp response_body({:ok, %{body: body, status: 200}}), do: body
  defp response_body(_error), do: :error
end
