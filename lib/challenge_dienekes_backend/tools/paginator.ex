defmodule ChallengeDienekesBackend.Tools.Paginator do
  def paginate(entries, params \\ %{}) do
    config = maybe_put_default_config(params)

    page = entries |> Scrivener.paginate(config)

    [
      entries: page.entries,
      page_number: page.page_number,
      page_size: page.page_size,
      total_pages: page.total_pages,
      total_entries: page.total_entries
    ]
  end

  defp maybe_put_default_config(%{page: _page_number, page_size: _page_size} = params), do: params

  defp maybe_put_default_config(%{"page" => page_number, "page_size" => page_size}),
    do: %{page: page_number, page_size: page_size}

  defp maybe_put_default_config(%{page: page_number} = _params),
    do: %{page: page_number, page_size: "100"}

  defp maybe_put_default_config(_params), do: %Scrivener.Config{page_number: 1, page_size: 100}
end
