defmodule ChallengeDienekesBackend.Tools.Feeder do
  use GenStage

  alias ChallengeDienekesBackendWeb.Integrations.DienekesApi

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(_page) do
    GenStage.start_link(__MODULE__, 1, name: __MODULE__)
  end

  @spec init(number) :: {:producer, number}
  def init(page) do
    {:producer, page}
  end

  @spec handle_demand(number, integer) :: {:noreply, list, number}
  def handle_demand(demand, page) when demand > 0 do
    %{"numbers" => events} = DienekesApi.fetch_numbers(page)

    {:noreply, events, page + 1}
  end
end
