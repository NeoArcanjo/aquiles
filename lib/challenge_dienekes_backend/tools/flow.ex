defmodule ChallengeDienekesBackend.Tools.Flow do
  use Flow

  alias ChallengeDienekesBackend.NumberCollection
  alias ChallengeDienekesBackend.Tools.Feeder

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(_) do
    out =
      Flow.from_stages([Feeder], min_demand: 1, max_demand: 100)
      |> Flow.partition()
      |> Flow.map(&NumberCollection.append/1)
      |> Flow.start_link()

    NumberCollection.sort()

    out
  end
end
