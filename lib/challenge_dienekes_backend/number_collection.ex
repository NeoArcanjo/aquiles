defmodule ChallengeDienekesBackend.NumberCollection do
  use Agent
  alias ChallengeDienekesBackend.Tools.{MergeSort, Paginator}

  @spec start_link(any) :: {:error, any} | {:ok, pid}
  def start_link(_) do
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  @spec value :: list
  def value do
    Agent.get(__MODULE__, & &1)
  end

  @spec value(map) :: list
  def value(params) do
    Agent.get(__MODULE__, &Paginator.paginate(&1, params))
    |> key_transform()
  end

  @spec sorted_value(map) :: list
  def sorted_value(params) do
    value()
    |> MergeSort.merge_sort()
    |> Paginator.paginate(params)
    |> key_transform()
  end

  @spec append(list) :: :ok
  def append(list) when is_list(list) do
    Agent.update(__MODULE__, &(&1 ++ list))
  end

  def append(number) do
    Agent.update(__MODULE__, &(&1 ++ [number]))
  end

  @spec sort :: :ok
  def sort do
    Agent.update(__MODULE__, &MergeSort.merge_sort(&1))
  end

  defp key_transform(kwl) do
    numbers = Keyword.get(kwl, :entries)

    kwl
    |> Keyword.drop([:entries])
    |> Keyword.put(:numbers, numbers)
  end
end
