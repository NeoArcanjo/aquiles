FROM elixir:1.12.2
LABEL Name=challengedienekesbackend Version=0.0.1
RUN apt-get -y update && apt-get install -y inotify-tools
RUN mix do local.hex --force, local.rebar --force, archive.install hex phx_new --force