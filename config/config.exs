# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :challenge_dienekes_backend, ChallengeDienekesBackendWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Vj/sP0LDQx38XaNP4Ym6vvzaCyFNQ3IiOuDteFnEOsNcaKIlg5/JMj3VRyRA0XDZ",
  render_errors: [view: ChallengeDienekesBackendWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: ChallengeDienekesBackend.PubSub,
  live_view: [signing_salt: "mQ8gaMcl"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :tesla, adapter: Tesla.Adapter.Hackney

if Mix.env() == :dev do
  config :git_hooks,
    auto_install: true,
    verbose: true,
    hooks: [
      pre_commit: [
        tasks: [
          {:cmd, "docker-compose run --rm challengedienekesbackend_test"}
        ]
      ],
      pre_push: [
        verbose: false,
        tasks: [
          {:cmd, "echo 'success!'"}
        ]
      ]
    ]
end

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
